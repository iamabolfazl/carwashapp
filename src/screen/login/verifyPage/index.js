import React from 'react'
import { View, Image, Text } from 'react-native'
import { Button } from 'react-native-elements'
import styles from './index.style'
import Logo from '../../../assets/images/Logo.png'
import Ellipsis from '../../../component/Ellipsis'
import Code from '../../../component/code/Code'

const Index = () => {
  return (
    <View style={styles.root}>
      <View style={styles.viewLogo}>
        <Image source={Logo} />
        <Text style={styles.txtLogo}>VERIFY YOUR MOBILE</Text>
        <Ellipsis />
      </View>
      <View style={styles.txtMain}>
        <Text style={styles.txtMain1}>
          Please review your registred email/Phone number for OTP.
        </Text>
        <Text style={styles.txtMain2}>Your OTP will expired on 15 minutes</Text>
      </View>
      <Code />
      <Button buttonStyle={styles.btn} title="Next" />
    </View>
  )
}

export default Index
