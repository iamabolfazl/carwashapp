import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  viewLogo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    width: 100,
    height: 100,
  },
  txtLogo: {
    color: '#363636',
    fontSize: 12,
  },
  txtMain: {
    width: '45%',
  },
  txtMain1: {
    color: '#9D9D9D',
    fontSize: 12,
    textAlign: 'center',
  },
  txtMain2: {
    color: '#9D9D9D',
    fontSize: 11,
    textAlign: 'center',
  },
  btn: {
    width: 230,
    borderRadius: 5,
    marginRight: 10,
  },
})
export default styles
