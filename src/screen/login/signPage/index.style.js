import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  viewLogo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtLogo: {
    color: '#363636',
    fontSize: 12,
  },
  btnView: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  titleStyleAccount: {
    fontSize: 12,
  },
  buttonStyleAccount: {
    backgroundColor: '#014F88',
  },
  inputView: {
    width: '75%',
  },
  titleStyleSign: {
    fontSize: 12,
  },
  buttonStyleSign: {
    width: 290,
    marginTop: 15,
  },
  txtAggre: {
    fontSize: 12,
    paddingTop: 10,
  },
  txtTerm: {
    color: '#00B4F2',
    fontSize: 12,
  },
  txtPrivacy: {
    bottom: 15,
    fontSize: 12,
    color: '#00B4F2',
  },
})
export default styles
