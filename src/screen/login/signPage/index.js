/* eslint-disable no-var */
// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Alert,
} from 'react-native'
import { Button } from 'react-native-elements'
import firebase from '@react-native-firebase/app'
import styles from './index.style'
import LogoS from '../../../assets/images/LogoS.png'
import Ellipsis from '../../../component/Ellipsis'
import InputSign from '../../../component/InputSign'
// eslint-disable-next-line import/imports-first
import '@react-native-firebase/auth'

const Index = ({ navigation }) => {
  const [btnDisabled, setBtnDisabled] = useState(true)
  const [email, setEmail] = useState('')
  const [mobile, setMobile] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const emailPattern = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g
  const mobilePattern = /^09[0-9]{9}$/
  useEffect(() => {
    if (
      emailPattern.test(email) &&
      mobilePattern.test(mobile) &&
      password.length > 5 &&
      confirmPassword.length > 5
    ) {
      setBtnDisabled(false)
    } else {
      setBtnDisabled(true)
    }
  }, [email, mobile, password, confirmPassword, emailPattern, mobilePattern])
  function onButtonPress() {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        navigation.navigate('Verify')
      })
      .catch(() => {
        Alert.alert('Sorry, Please Try Again')
      })
  }
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="height">
      <View style={styles.root}>
        <View style={styles.viewLogo}>
          <Image source={LogoS} />
          <Text style={styles.txtLogo}>CREATE AN ACCOUNT</Text>
          <Ellipsis />
        </View>
        <View style={styles.inputView}>
          <InputSign title="First Name" />
          <InputSign title="Last Name" />
          <InputSign
            title="Email"
            value={email}
            onChangeText={(txt) => {
              setEmail(txt)
            }}
          />
          <InputSign
            title="Password"
            secureTextEntry
            value={password}
            onChangeText={(txt) => {
              setPassword(txt)
            }}
          />
          <InputSign
            title="Confirm Password"
            secureTextEntry
            value={confirmPassword}
            onChangeText={(txt) => {
              setConfirmPassword(txt)
            }}
          />
          <InputSign
            title="Phone"
            value={mobile}
            onChangeText={(txt) => {
              setMobile(txt)
            }}
          />
        </View>
        <View>
          <Button
            disabled={btnDisabled}
            onPress={onButtonPress}
            title="Sign Up"
            titleStyle={styles.titleStyleSign}
            buttonStyle={styles.buttonStyleSign}
          />
        </View>
        <View
          style={{
            width: '80%',
            height: 100,
            alignItems: 'center',
          }}
        >
          <TouchableOpacity>
            <Text style={styles.txtAggre}>
              by signing up you agree with the
              <Text style={styles.txtTerm}>Terms of Service</Text>
            </Text>
            <Text> </Text>
          </TouchableOpacity>

          <TouchableOpacity>
            <Text style={styles.txtPrivacy}>Privacy Policy</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.btnView}>
          <Button
            onPress={() => navigation.navigate('Login')}
            title="Already have an account?"
            titleStyle={styles.titleStyleAccount}
            buttonStyle={styles.buttonStyleAccount}
          />
        </View>
      </View>
    </KeyboardAvoidingView>
  )
}

export default Index
