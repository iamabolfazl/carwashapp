import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtLogin: {
    color: '#363636',
    marginTop: 80,
  },
  forgotView: {
    paddingLeft: 50,
    alignSelf: 'flex-start',
  },
  txt: {
    color: '#000',
  },
  img: {},
  imgView: {
    marginBottom: -200,
  },
})
export default styles
