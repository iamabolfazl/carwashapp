import React, { useContext, useState, useEffect } from 'react'
import { View, Text, Image } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import firebase from '@react-native-firebase/app'
import styles from './index.style'
import Ellipsis from '../../../component/Ellipsis'
import InputComponent from '../../../component/InputComponent'
import ButtonComponent from '../../../component/ButtonComponent'
import LoginPic from '../../../assets/images/LoginPic.png'
import { LoginContext } from '../../../context/LoginContext'
// eslint-disable-next-line import/imports-first
import '@react-native-firebase/auth'

const Index = ({ navigation }) => {
  const { login } = useContext(LoginContext)
  // const SignIn = () => {
  //   login('ABolfazl')
  // }

  const [btnDisable, setBtnDisable] = useState(true)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  useEffect(() => {
    if (email != null && password.length > 6) {
      setBtnDisable(false)
    } else {
      setBtnDisable(true)
    }
  }, [email, password])
  function onButtonPress() {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        login('email')
      })
      .catch(() => {
        navigation.navigate('SignUp')
      })
  }

  return (
    <View style={styles.root}>
      <Text style={styles.txtLogin}>LOGIN</Text>
      <Ellipsis />
      <InputComponent
        title="Email"
        value={email}
        onChangeText={(txt) => {
          setEmail(txt)
        }}
      />
      <InputComponent
        secureTextEntry
        title="Password"
        value={password}
        onChangeText={(txt) => {
          setPassword(txt)
        }}
      />
      <View style={styles.forgotView}>
        <TouchableOpacity>
          <Text style={styles.txt}>Forgot Password</Text>
        </TouchableOpacity>
      </View>
      <ButtonComponent
        disabled={btnDisable}
        nav={onButtonPress}
        title="Sign In"
        bgColor="#1CA9F3"
      />
      <ButtonComponent
        nav={() => navigation.navigate('SignUp')}
        title="Dont have an account? Sign Up"
        bgColor="#014F88"
      />
      <View style={styles.imgView}>
        <Image source={LoginPic} style={styles.img} />
      </View>
    </View>
  )
}

export default Index
