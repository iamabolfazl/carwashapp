/* eslint-disable no-use-before-define */
import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
// import SplashScreenPage from '../assets/images/Splash.jpg'

const SplashScreen = () => {
  return (
    <View style={styles.root}>
      <Image
        source={require('../assets/images/Spalsh.jpg')}
        style={styles.image}
      />
    </View>
  )
}

export default SplashScreen
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: { width: '100%', height: '100%' },
})
