import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    color: '#000',
    fontSize: 14,
    marginLeft: 50,
  },
  mainView: {
    backgroundColor: '#F7F9FD',
    flex: 1,
    borderTopRightRadius: 150,
    marginTop: 10,
  },
  stationView: {
    margin: 20,
  },
  stationTxt: {
    fontSize: 16,
  },
  txtView: {
    margin: 20,
  },
  iconLike: {
    alignSelf: 'flex-end',
    color: 'red',
  },
  txtLocation: {
    opacity: 0.5,
    paddingTop: 10,
    bottom: 30,
  },
  br: {
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    marginTop: 30,
    bottom: 40,
  },
})
export default styles
