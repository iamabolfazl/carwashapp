import React from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'native-base'
import styles from './index.style'
import Header from '../../../component/Header'

const Index = () => {
  return (
    <View style={styles.root}>
      <View>
        <Header title="MY FAIVORT" style={styles.header} />
      </View>
      <View style={styles.mainView}>
        <View style={styles.stationView}>
          <Text style={styles.stationTxt}>Station</Text>
        </View>
        <View style={styles.txtView}>
          <Text>Uptown Service Station</Text>
          <Icon style={styles.iconLike} name="heart" type="Ionicons" />
          <Text style={styles.txtLocation}>Tabriz</Text>
          <View style={styles.br} />
        </View>
        <View style={styles.txtView}>
          <Text>Uptown Service Station</Text>
          <Icon style={styles.iconLike} name="heart" type="Ionicons" />
          <Text style={styles.txtLocation}>Tabriz</Text>
          <View style={styles.br} />
        </View>
      </View>
    </View>
  )
}

export default Index
