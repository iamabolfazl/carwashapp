import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { LoginContext } from '../../../context/LoginContext'

const Index = () => {
  const { logout } = useContext(LoginContext)
  return (
    <View>
      <TouchableOpacity onPress={logout}>
        <Text>Others</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Index
