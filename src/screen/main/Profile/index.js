import React, { useContext } from 'react'
import { View, Image, Text, TouchableOpacity } from 'react-native'
import { Icon, Button, List, ListItem, Left, Right } from 'native-base'
import styles from './index.style'
import Header from '../../../component/Header'
import ProfilePic from '../../../assets/images/Profile.png'
import { LoginContext } from '../../../context/LoginContext'

const Index = ({ navigation }) => {
  const { logout } = useContext(LoginContext)
  return (
    <View style={styles.root}>
      <Header title=" PROFIEL" style={styles.header} />
      <View style={styles.profile}>
        <Image source={ProfilePic} style={styles.profilePic} />
        <TouchableOpacity>
          <View style={styles.cameraView}>
            <Icon name="camera" type="Ionicons" style={styles.cameraIcon} />
          </View>
        </TouchableOpacity>
        <Button
          onPress={() => navigation.navigate('Notification')}
          style={styles.btnView}
        >
          <Icon name="notifications" type="Ionicons" style={styles.btnIcon} />
        </Button>
        <View style={styles.info}>
          <Text style={styles.txtInfo1}>Abolfazl Masoumi</Text>
          <Text style={styles.txtInfo2}>iamabolfazlmasoumi@gmail.com</Text>
          <Text style={styles.txtInfo3}>Active sice Octobre 2020</Text>
        </View>
      </View>
      <View style={styles.list}>
        <List>
          <ListItem onPress={() => navigation.navigate('SavedCard')} noIndent>
            <Left>
              <Text>Saved Cards</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <ListItem
            onPress={() => navigation.navigate('PaymentHistory')}
            noIndent
          >
            <Left>
              <Text>Payment History</Text>
            </Left>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </ListItem>
          <View style={{ margin: 20, marginTop: 40 }}>
            <TouchableOpacity onPress={logout}>
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>
        </List>
      </View>
    </View>
  )
}

export default Index
