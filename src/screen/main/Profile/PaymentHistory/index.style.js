import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    color: '#000',
    fontSize: 14,
    marginLeft: 20,
  },
  mainView: {
    backgroundColor: '#F7F9FD',
    flex: 1,
    borderTopRightRadius: 150,
    marginTop: 10,
  },
})
export default styles
