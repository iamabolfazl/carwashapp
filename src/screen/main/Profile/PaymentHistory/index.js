import React from 'react'
import { View } from 'react-native'
import styles from './index.style'
import Header from '../../../../component/Header'

const Index = () => {
  return (
    <View style={styles.root}>
      <View>
        <Header
          title="PAYMENT HISTORY"
          style={styles.header}
          IconName="arrow-back"
        />
      </View>
      <View style={styles.mainView} />
    </View>
  )
}

export default Index
