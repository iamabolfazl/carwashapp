import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    color: '#000',
    fontSize: 14,
    marginLeft: 30,
  },
  mainView: {
    backgroundColor: '#F7F9FD',
    flex: 1,
    borderTopRightRadius: 150,
    marginTop: 10,
  },
  cardView: {
    width: '80%',
    alignSelf: 'center',
  },
  masterImage: {
    width: 35,
    height: 35,
    marginRight: 20,
    marginTop: 30,
  },
  txtView: {
    flexDirection: 'row',
  },
  txtNum: {
    paddingTop: 20,
    fontSize: 12,
    flex: 1,
  },
  txtDefault: {
    paddingTop: 20,
    fontSize: 12,
  },
  containerStyle: {
    right: 10,
  },
  inputStyle: {
    fontSize: 12,
  },
  btnView: {
    width: '75%',
    alignSelf: 'center',
    marginRight: 20,
  },
  titleStyle: {
    fontSize: 12,
  },
})
export default styles
