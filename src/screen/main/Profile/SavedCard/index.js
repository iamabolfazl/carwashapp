import React from 'react'
import { View, Image, Text } from 'react-native'
import { Input, Button } from 'react-native-elements'
import styles from './index.style'
import Header from '../../../../component/Header'
import Master from '../../../../assets/images/masterLogo.png'

const Index = () => {
  return (
    <View style={styles.root}>
      <View>
        <Header
          title="SAVED CARDS"
          style={styles.header}
          IconName="arrow-back"
        />
      </View>
      <View style={styles.mainView}>
        <View style={styles.cardView}>
          <Image source={Master} style={styles.masterImage} />
          <View style={styles.txtView}>
            <Text style={styles.txtNum}>4532 8463 8390 2820</Text>
            <Text style={styles.txtDefault}>default</Text>
          </View>
          <Input
            containerStyle={styles.containerStyle}
            inputStyle={styles.inputStyle}
          />
        </View>
        <View style={styles.btnView}>
          <Button title="Add New Card" titleStyle={styles.titleStyle} />
        </View>
      </View>
    </View>
  )
}

export default Index
