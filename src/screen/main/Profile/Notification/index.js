import React, { useState } from 'react'
import { View, Text } from 'react-native'
import { ListItem, Body, Right, Switch } from 'native-base'
import styles from './index.style'
import Header from '../../../../component/Header'

const Index = () => {
  const [offer, setOffer] = useState(false)
  const [booking, setBooking] = useState(false)
  const [newService, setNewService] = useState(false)

  const onToogleSwitchOffer = () => {
    setOffer(!offer)
  }
  const onToogleSwitchBooking = () => {
    setBooking(!booking)
  }
  const onToogleSwitchNewService = () => {
    setNewService(!newService)
  }

  return (
    <View style={styles.root}>
      <View>
        <Header
          title="NOTIFICATION"
          style={styles.header}
          IconName="arrow-back"
        />
      </View>
      <View style={styles.mainView}>
        <ListItem itemDivider>
          <Text style={{ fontSize: 10 }}>Push Notification</Text>
        </ListItem>
        <ListItem style={{ marginTop: 20 }} icon>
          <Body>
            <Text>Offer Details</Text>
          </Body>
          <Right>
            <Switch value={offer} onValueChange={onToogleSwitchOffer} />
          </Right>
        </ListItem>
        <ListItem style={{ marginTop: 20 }} icon>
          <Body>
            <Text>Booking Details</Text>
          </Body>
          <Right>
            <Switch value={booking} onValueChange={onToogleSwitchBooking} />
          </Right>
        </ListItem>
        <ListItem style={{ marginTop: 20 }} icon>
          <Body>
            <Text>New Service Station Alert</Text>
          </Body>
          <Right>
            <Switch
              value={newService}
              onValueChange={onToogleSwitchNewService}
            />
          </Right>
        </ListItem>
      </View>
    </View>
  )
}

export default Index
