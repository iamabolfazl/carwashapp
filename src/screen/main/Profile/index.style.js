import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  header: {
    paddingLeft: 50,
    color: '#000',
    fontSize: 14,
  },
  profile: {
    flex: 1,
    backgroundColor: '#F7F9FD',
    width: '100%',
    height: '50%',
    borderTopRightRadius: 200,
    marginTop: 10,
  },

  list: {
    flex: 1,
    backgroundColor: '#FFF',
    width: '100%',
  },
  cameraView: {
    width: 30,
    height: 30,
    backgroundColor: '#FFF',
    position: 'absolute',
    bottom: 5,
    left: 55,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraIcon: {
    color: '#9C9C9C',
    fontSize: 20,
  },
  profilePic: {
    width: 100,
    height: 100,
    margin: 20,
  },
  btnView: {
    width: 50,
    height: 100,
    backgroundColor: '#1875FC',
    position: 'absolute',
    bottom: 175,
    left: 125,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnIcon: {
    color: '#FFF',
    fontSize: 20,
  },
  info: {},
  txtInfo1: {
    paddingLeft: 20,
    fontSize: 16,
  },
  txtInfo2: {
    paddingLeft: 20,
  },
  txtInfo3: {
    paddingTop: 20,
    paddingLeft: 20,
    fontSize: 12,
    color: '#6C6D6F',
  },
})
export default styles
