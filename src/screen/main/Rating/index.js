import React from 'react'
import { View, Text, Image } from 'react-native'
import { Button } from 'react-native-elements'
import styles from './index.style'
import LogoS from '../../../assets/images/LogoS.png'
import Ellipsis from '../../../component/Ellipsis'
import Rating from '../../../component/Rating/StarRating'

const Index = ({ navigation }) => {
  return (
    <View style={styles.root}>
      <View style={styles.viewLogo}>
        <Image source={LogoS} />
        <Text style={styles.txtLogo}>REVIEW</Text>
        <Ellipsis />
      </View>
      <View style={styles.backView}>
        <View style={styles.backRatingView}>
          <Text style={styles.txtRating}>
            Thank you for giving opportunity to serve you. Please rate our
            service center Sears and give your valuable feedback
          </Text>
          <Rating />
          <View style={styles.backSubmitView}>
            <Button
              onPress={() => navigation.navigate('Home')}
              title="Submit"
              buttonStyle={styles.buttonStyle}
              titleStyle={styles.titleStyle}
              containerStyle={styles.containerStyle}
            />
          </View>
        </View>
      </View>
    </View>
  )
}

export default Index
