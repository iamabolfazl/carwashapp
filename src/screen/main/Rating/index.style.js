import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
    paddingTop: 15,
  },
  viewLogo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtLogo: {
    color: '#363636',
    fontSize: 12,
  },
  backView: {
    width: '100%',
    flex: 1,
    backgroundColor: '#F6F8FB',
    borderTopLeftRadius: 150,
    marginTop: 10,
  },
  backRatingView: {
    backgroundColor: '#1CA9F3',
    width: '60%',
    height: '60%',
    alignSelf: 'center',
    top: 80,
    borderRadius: 50,
    elevation: 20,
  },
  backSubmitView: {
    backgroundColor: '#fff',
    width: '70%',
    height: '60%',
    alignSelf: 'center',
    top: 50,
    borderRadius: 10,
    elevation: 50,
  },
  txtRating: {
    color: '#FFFFFF',
    fontSize: 10,
    width: '85%',
    textAlign: 'center',
    alignSelf: 'center',
    paddingTop: 20,
  },
  buttonStyle: {
    backgroundColor: 'transparent',
    borderTopWidth: 1,
    borderColor: '#D8D8D8',
  },
  titleStyle: {
    color: '#014F88',
    fontSize: 10,
  },
  containerStyle: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
})
export default styles
