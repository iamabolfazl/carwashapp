import { StyleSheet, Platform } from 'react-native'

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    flex: 1,
  },
  textInputView: {
    position: 'absolute',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  textInput: {
    flex: 1,
    padding: 0,
  },
  iconSearch: {
    fontSize: 14,
    alignSelf: 'flex-end',
    bottom: 20,
    right: 20,
  },
  flatView: {
    flexDirection: 'row',
    paddingRight: 20,
    position: 'absolute',
    bottom: 50,
  },
})

export default styles
