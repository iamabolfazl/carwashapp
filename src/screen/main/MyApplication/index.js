/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import { View, FlatList } from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
// import { View } from 'react-native'
import { Icon } from 'native-base'
import { Input } from 'react-native-elements'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import styles from './index.style'
import Header from '../../../component/Header'
import StoreService from '../../../component/Store/StoreService'

const Index = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: 1,
      imageuri:
        'https://images.theconversation.com/files/76578/original/image-20150331-1231-1ttwii6.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=1200.0&fit=crop',
      txtTitle: 'Tehran',
      txtRegion: 'saadat abad 12',
    },
    {
      id: 2,
      imageuri:
        'https://cdn2.lamag.com/wp-content/uploads/sites/6/2019/01/carwash-1068x715.jpg',
      txtTitle: 'Tabriz',
      txtRegion: 'Sarab 125',
    },
    {
      id: 3,
      imageuri:
        'https://i.guim.co.uk/img/media/2ad114b11497f26aa9707389ebae56a4ad7c630b/0_0_7360_4417/master/7360.jpg?width=700&quality=85&auto=format&fit=max&s=f7dee351bacdb68723576885de53b66e',
      txtTitle: 'Rasht',
      txtRegion: 'gilan 468',
    },
    {
      id: 4,
      imageuri:
        'https://www.vikingmergers.com/wp-content/uploads/2015/02/Carwash-4844x2422.jpg',
      txtTitle: 'Amol',
      txtRegion: 'mazandaran 834',
    },
    {
      id: 5,
      imageuri:
        'https://cdn2.lamag.com/wp-content/uploads/sites/6/2019/01/carwash-1068x715.jpg',
      txtTitle: 'Kordestan',
      txtRegion: 'sannadaj 789',
    },
  ])
  return (
    <>
      <View>
        <Header
          title="CAR SERVICE CENTER"
          style={{ paddingLeft: 20, color: '#000', fontSize: 14 }}
        />
      </View>
      <View style={{ flex: 1 }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={{
            latitude: 35.56818723424372,
            longitude: 51.15058469834094,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
        />
        {/* <View style={styles.textInputView}>
          <Input
            autoCapitalize="none"
            placeholder="Search service center"
            style={styles.textInput}
          />
          <Icon style={styles.iconSearch} name="search" type="Ionicons" />
        </View> */}
        <View style={styles.flatView}>
          <FlatList
            data={data}
            showsHorizontalScrollIndicator={false}
            horizontal
            renderItem={({ item }) => (
              <StoreService
                imageURL={item.imageuri}
                txtTitle={item.txtTitle}
                txtRegion={item.txtRegion}
              />
            )}
          />
        </View>
      </View>
    </>
  )
}
export default Index
