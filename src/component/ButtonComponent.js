/* eslint-disable no-var */
/* eslint-disable no-use-before-define */
import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Button } from 'react-native-elements'

const styles = StyleSheet.create({
  btnView: {
    marginTop: 20,
  },
  txt: {
    fontSize: 14,
  },
})

const ButtonComponent = ({ title, bgColor, nav, disabled }) => {
  var disabledState = disabled != null ? disabled : false
  return (
    <View style={styles.btnView}>
      <Button
        disabled={disabledState}
        onPress={nav}
        title={title}
        buttonStyle={{
          width: 320,
          borderRadius: 5,
          height: 40,
          backgroundColor: bgColor,
        }}
        titleStyle={styles.txt}
      />
    </View>
  )
}

export default ButtonComponent
