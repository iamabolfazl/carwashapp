import React from 'react'
import { View } from 'react-native'
import { AirbnbRating } from 'react-native-ratings'

const StarRating = ({ size, style }) => {
  return (
    <View>
      <AirbnbRating
        reviews={[]}
        count={5}
        defaultRating={5}
        size={size}
        showRating
        starStyle={style}
      />
    </View>
  )
}

export default StarRating
