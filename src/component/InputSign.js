/* eslint-disable no-use-before-define */
import React from 'react'
import { StyleSheet } from 'react-native'
import { Input } from 'react-native-elements'

const InputSign = ({
  title,
  onBlur,
  onFocus,
  secureTextEntry,
  value,
  onChangeText,
}) => {
  return (
    <Input
      label={title}
      labelStyle={styles.labelStyle}
      inputContainerStyle={styles.inputContainerStyle}
      containerStyle={styles.containerStyle}
      secureTextEntry={secureTextEntry}
      onBlur={onBlur}
      onFocus={onFocus}
      inputStyle={styles.inputStyle}
      value={value}
      onChangeText={onChangeText}
    />
  )
}

export default InputSign
const styles = StyleSheet.create({
  labelStyle: {
    fontSize: 12,
    paddingBottom: 5,
  },
  inputContainerStyle: {
    borderWidth: 1,
    borderColor: '#D9D9D9',
    borderRadius: 5,
    height: 35,
  },
  containerStyle: {
    height: 60,
  },
  inputStyle: {
    fontSize: 12,
  },
})
