/* eslint-disable no-use-before-define */
import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { Input } from 'react-native-elements'

const InputComponent = ({ title, value, onChangeText, secureTextEntry }) => {
  return (
    <View>
      <Text style={styles.txt}>{title}</Text>
      <Input
        containerStyle={styles.containrtStyle}
        inputContainerStyle={styles.inputContainerStyle}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        inputStyle={styles.inputStyle}
      />
    </View>
  )
}

export default InputComponent
const styles = StyleSheet.create({
  containrtStyle: {},
  inputContainerStyle: {
    width: '80%',
    height: 40,
    borderWidth: 1,
    borderColor: '#D9D9D9',
    borderRadius: 5,
  },
  txt: {
    padding: 10,
    color: '#5D5D5D',
  },
  inputStyle: {
    fontSize: 12,
  },
})
