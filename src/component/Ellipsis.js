/* eslint-disable no-use-before-define */
import React from 'react'
import { StyleSheet } from 'react-native'
import AnimatedEllipsis from 'react-native-animated-ellipsis'

const Ellipsis = () => {
  return (
    <AnimatedEllipsis
      numberOfDots={5}
      animationDelay={250}
      style={styles.ellipsis}
    />
  )
}

export default Ellipsis
const styles = StyleSheet.create({
  ellipsis: {
    color: '#71D9FC',
    fontSize: 30,
  },
})
