/* eslint-disable no-use-before-define */
import React from 'react'
import { StyleSheet } from 'react-native'
import { Header, Left, Body, Button, Icon, Title } from 'native-base'
import { useNavigation } from '@react-navigation/native'

const HeaderComponent = ({ title, IconName, style }) => {
  const navigation = useNavigation()
  return (
    <Header style={styles.header}>
      <Left>
        <Button transparent onPress={() => navigation.goBack()}>
          <Icon name={IconName} style={styles.icon} />
        </Button>
      </Left>
      <Body>
        <Title style={style}>{title}</Title>
      </Body>
    </Header>
  )
}

export default HeaderComponent
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFF',
  },
  icon: {
    fontSize: 18,
    color: '#363636',
  },
  title: {
    fontSize: 14,
    paddingLeft: 10,
    color: '#363636',
  },
})
