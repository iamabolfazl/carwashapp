/* eslint-disable no-use-before-define */
import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import Rating from '../Rating/StarRating'

const StoreService = ({ txtTitle, txtRegion, imageURL }) => {
  return (
    <TouchableOpacity>
      <View style={styles.mainView}>
        <Image
          source={{
            uri: imageURL,
          }}
          style={styles.image}
        />
        <View style={styles.txtView}>
          <Text style={styles.txtTitle}>{txtTitle}</Text>
          <Text style={styles.txtRegion}>{txtRegion}</Text>
          <Rating size={10} style={styles.rating} />
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default StoreService
const styles = StyleSheet.create({
  mainView: {
    backgroundColor: '#FFF',
    width: 150,
    height: 200,
    margin: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  image: {
    marginTop: 10,
    width: 130,
    height: 80,
  },
  txtView: {
    backgroundColor: '#1CA9F3',
    width: 150,
    height: 100,
    marginTop: 10,
    borderTopLeftRadius: 50,
  },
  txtTitle: {
    color: '#FFF',
    left: 15,
    fontSize: 14,
    top: 20,
    fontWeight: 'bold',
  },
  txtRegion: {
    color: '#FFF',
    left: 15,
    fontSize: 10,
    top: 25,
    fontWeight: 'bold',
  },
  rating: {
    bottom: 20,
    right: 20,
  },
})
