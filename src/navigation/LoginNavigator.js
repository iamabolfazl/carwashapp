import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Login from '../screen/login/loginPage'
import SignUp from '../screen/login/signPage'
import Verify from '../screen/login/verifyPage'

const Stack = createStackNavigator()
const LoginNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Login" headerMode="none">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen name="Verify" component={Verify} />
    </Stack.Navigator>
  )
}

export default LoginNavigator
