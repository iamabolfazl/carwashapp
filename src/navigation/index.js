/* eslint-disable no-nested-ternary */
/* eslint-disable import/named */
import React, { useContext } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import LoginNavigator from './LoginNavigator'
import { LoginContext } from '../context/LoginContext'
import MainNavigator from './MainNavigator'
import Splash from '../screen/SplashScreen'

const Index = () => {
  const { isLoggedIn, loading } = useContext(LoginContext)
  return (
    <NavigationContainer>
      {loading ? (
        <Splash />
      ) : isLoggedIn ? (
        <MainNavigator />
      ) : (
        <LoginNavigator />
      )}
      {/* {isLoggedIn ? <MainNavigator /> : <LoginNavigator />} */}
    </NavigationContainer>
  )
}

export default Index
