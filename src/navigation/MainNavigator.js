import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'
import MyApplication from '../screen/main/MyApplication'
import MyFaivort from '../screen/main/MyFaivort'
// import Others from '../screen/main/Others'
import Profile from '../screen/main/Profile'
import SavedCard from '../screen/main/Profile/SavedCard'
import PaymentHistory from '../screen/main/Profile/PaymentHistory'
import Notification from '../screen/main/Profile/Notification'
import Search from '../screen/main/Search'
import Rating from '../screen/main/Rating'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

export const MyApplicationStack = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="MyApplication" component={MyApplication} />
      <Stack.Screen name="Search" component={Search} />
    </Stack.Navigator>
  )
}
export const MyFaivortStack = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="MyFaivort" component={MyFaivort} />
    </Stack.Navigator>
  )
}
export const OthersStack = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Rating" component={Rating} />
    </Stack.Navigator>
  )
}
export const ProfileStack = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="SavedCard" component={SavedCard} />
      <Stack.Screen name="PaymentHistory" component={PaymentHistory} />
      <Stack.Screen name="Notification" component={Notification} />
    </Stack.Navigator>
  )
}

const LoginNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="MyApp"
        component={MyApplicationStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="square"
              size={25}
              color={focused ? '#1CA9F3' : '#707070'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="MyFaivort"
        component={MyFaivortStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="star"
              size={25}
              color={focused ? '#1CA9F3' : '#707070'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Other"
        component={OthersStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="shield"
              size={25}
              color={focused ? '#1CA9F3' : '#707070'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="ellipse"
              size={25}
              color={focused ? '#1CA9F3' : '#707070'}
            />
          ),
        }}
      />
    </Tab.Navigator>
  )
}

export default LoginNavigator
