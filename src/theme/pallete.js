export default {
  PrimitiveColor: '#1CA9F3',
  SecondaryColor: '#014F88',
  TextLoginTitle: '#363636',
  SubTextLoginMain: '#9D9D9D',
}
