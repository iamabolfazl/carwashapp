/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import firebase from '@react-native-firebase/app'
import Index from './src/navigation'
import LoginContextProvider from './src/context/LoginContext'
// eslint-disable-next-line import/imports-first
import '@react-native-firebase/auth'

const App = () => {
  const firebaseConfig = {
    apiKey: 'AIzaSyBBTXu4HEvg-G1308q-TaWpW8DUUqO-RsQ',
    authDomain: 'carwash-887d2.firebaseapp.com',
    databaseURL: 'https://carwash-887d2.firebaseio.com',
    projectId: 'carwash-887d2',
    storageBucket: 'carwash-887d2.appspot.com',
    messagingSenderId: '520193353091',
    appId: '1:520193353091:web:b3434dbdb658aa489142b6',
  }

  useEffect(() => {
    firebase.initializeApp(firebaseConfig)
  }, [])
  return (
    <LoginContextProvider>
      <Index />
    </LoginContextProvider>
  )
}

export default App
